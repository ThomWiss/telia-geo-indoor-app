import {LimiterFilter, LimitFilterInterface, LowPassFilter, LowPassFilterInterface} from "./filters";

export class Beacon {
    private lowPassFilter: LowPassFilterInterface;
    private limiterFilter: LimitFilterInterface;
    private lastSeen: number;
    private rssis: Array<number> = new Array<number>();

    constructor(public name, public latitude: number, public longitude: number, private macAddress: string) {
        this.lowPassFilter = new LowPassFilter(8);
        this.limiterFilter = new LimiterFilter(2,2);
    }

    setRssi(rssi:number, txLevel: number){
        this.lowPassFilter.setRssi(rssi);
        //let filteredRssi = this.lowPassFilter.getFilteredRssi();
        //let distance = distanceLogTx(filteredRssi,txLevel);
        //let distance = distanceLog(rssi);


        //this.limiterFilter.setDistance(distance);
        this.lastSeen = Date.now();
        //this.rssis.push(rssi);

    }

    getDistance(){

        return distanceLog(this.lowPassFilter.getFilteredRssi());
        //return this.limiterFilter.getFilteredDistance();
    }

    getRssis() : Array<number>{
        return this.rssis;
    }

    getLastSeen(){
        return this.lastSeen;
    }
    getMacAddress(){
        return this.macAddress;
    }

}

export function distanceLog(rssi) {
    let x = -52 - rssi;
    return Math.pow(10, x / 18);
    //return 10^(x/20)
}

export function distanceLogTx(rssi,txPower) {
    let x = txPower - rssi;
    return Math.pow(10, x / 25);
    //return 10^(x/20)
}
