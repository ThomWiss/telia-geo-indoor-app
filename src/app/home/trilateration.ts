import * as Math from 'mathjs';
import {Beacon} from "./beacon";
import {ILatLng, LatLng} from '@ionic-native/google-maps';

export namespace Trilateration {



   /* export function testGetPositionViaTrilateration(): void {

        let beacon1 = {latitude: 62.384113725868220, longitude: 17.312242984771725, distance: 75};
        let beacon2 = {latitude: 62.384362387999765, longitude: 17.314496040344240, distance: 75};
        let beacon3 = {latitude: 62.382979800396924, longitude: 17.313584089279175, distance: 75};

        let position = Trilateration.getPositionByTrilateration(beacon1, beacon2, beacon3);
        console.log(position.latitude, position.longitude);
    }*/

    export function getPosition(beacons: Array<Beacon>, lastKnownPosition: ILatLng): ILatLng {
        if(beacons.length < 3){
            return null;
        } else {
            return Trilateration.getPositionByTrilateration(lastKnownPosition, beacons[0], beacons[1], beacons[2]);
        }
    }

    export function getPositionByTrilateration(lastKnownPosition: ILatLng, beacon1: Beacon, beacon2: Beacon, beacon3: Beacon): ILatLng {

        const maxMovement:number = 2; //maximum movement

        let positions = new Array();
        let pos1 = Trilateration.trilateration(beacon1, beacon2, beacon3);
        let pos2 = Trilateration.trilateration(beacon1, beacon3, beacon2);
        let pos3 = Trilateration.trilateration(beacon2, beacon3, beacon1);

        positions.push({position: pos1, distanceToLastKnownPos: Trilateration.distanceInMeters(pos1.lat,pos1.lng, lastKnownPosition.lat,lastKnownPosition.lng)})
        positions.push({position: pos2, distanceToLastKnownPos: Trilateration.distanceInMeters(pos2.lat,pos2.lng, lastKnownPosition.lat,lastKnownPosition.lng)})
        positions.push({position: pos3, distanceToLastKnownPos: Trilateration.distanceInMeters(pos3.lat,pos3.lng, lastKnownPosition.lat,lastKnownPosition.lng)})

        //Sort by closest distance
        let closestPos = positions.sort((a, b) => a.distanceToLastKnownPos - b.distanceToLastKnownPos)[0];

        if(closestPos.distanceToLastKnownPos > maxMovement && closestPos.distanceToLastKnownPos < 100){
            let ratio = maxMovement/closestPos.distanceToLastKnownPos;
            closestPos.position.lat = lastKnownPosition.lat + (closestPos.position.lat-lastKnownPosition.lat)*ratio;
            closestPos.position.lng =  lastKnownPosition.lng + (closestPos.position.lng-lastKnownPosition.lng)*ratio;
        }

        return closestPos.position;
        /*
        let lat = (pos1.latitude + pos2.latitude) * 0.5;
        let lon = (pos1.longitude + pos2.longitude) * 0.5;
        return  new LatLng(lat,lon);
        */

    }

    export function trilateration(beacon1: Beacon, beacon2: Beacon, beacon3: Beacon) :ILatLng{

        let dg1 = beacon1.getDistance(); // distance between gamer and beacon1 in meters
        let dg2 = beacon2.getDistance(); // distance between gamer and beacon2 in meters
        let dg3 = beacon3.getDistance(); // distance between gamer and beacon3 in meters
        let d12 = Trilateration.distanceInMeters(beacon1.latitude, beacon1.longitude, beacon2.latitude, beacon2.longitude);

        // fix bad input - to small rings
        if (dg1 + dg2 < d12) {
            let t = (d12 - dg1 - dg2) + 1;
            dg1 += (dg1/d12)*t;
            dg2 += (dg2/d12)*t;
        }

        // fix bad input - to large ring
        if( dg1+d12 < dg2 ) {
            dg2=dg1+d12-1;
        }
        if( dg2+d12 < dg1 ) {
            dg1=dg2+d12-1;
        }

        // calculate x,y in beacon1 local space
        let xlen = ((dg1 * dg1) - (dg2 * dg2) + (d12 * d12)) / (d12 + d12);
        let ylen = 0;
        //console.log("xlen: ",xlen);
        //if(xlen >= 0){
            ylen = Math.sqrt((dg1 * dg1) - (xlen * xlen));
        //}else{
            //ylen = Math.sqrt((dg2 * dg2) - (xlen * xlen));
        //}


        // create a b1-b2-vector
        let vx = Trilateration.distanceInMeters(beacon1.latitude, beacon1.longitude, beacon1.latitude, beacon2.longitude) / d12;
        let vy = Trilateration.distanceInMeters(beacon1.latitude, beacon1.longitude, beacon2.latitude, beacon1.longitude) / d12;

        // neg checks
        if (beacon1.longitude > beacon2.longitude) {
            vx = -vx;
        }
        if (beacon1.latitude > beacon2.latitude) {
            vy = -vy;
        }

        // calculate temporary point on 1-2-vector
        let tx = vx * xlen;
        let ty = vy * xlen;

        // calculate vector between gamer and temporary point, by rotating b1-b2-vector 90 degrees
        let gvx = -vy;
        let gvy = vx;

        // calculate possible gamer points
        let gx1 = gvx * ylen + tx;
        let gy1 = gvy * ylen + ty;
        let gx2 = gvx * (-ylen) + tx;
        let gy2 = gvy * (-ylen) + ty;

        // transform x,y to world space
        let lon1 = Trilateration.meterToLon(gx1, beacon1.longitude, beacon1.latitude) + beacon1.longitude;
        let lat1 = Trilateration.meterToLat(gy1, beacon1.latitude) + beacon1.latitude;
        let lon2 = Trilateration.meterToLon(gx2, beacon1.longitude, beacon1.latitude) + beacon1.longitude;
        let lat2 = Trilateration.meterToLat(gy2, beacon1.latitude) + beacon1.latitude;

        // Use beacon3 to select which point to return
        if (Math.abs(Trilateration.distanceInMeters(beacon3.latitude, beacon3.longitude, lat1, lon1) - dg3) > Math.abs(Trilateration.distanceInMeters(beacon3.latitude, beacon3.longitude, lat2, lon2) - dg3)) {
            return new LatLng(lat2,lon2);
            //return {latitude: lat2, longitude: lon2};
        } else {
            return new LatLng(lat1,lon1);
            //return {latitude: lat1, longitude: lon1,};
        }
    }

    export function distanceInMeters(lat1: any, lon1: any, lat2: any, lon2: any) {

        // calculate a mean value for latitude
        let mlat = Math.abs((lat2 + lat1) * 0.5);

        // in radians
        let rlat = Trilateration.toRadians(mlat);

        // calculate latitude scalar
        let scalarLat = 110574 + (mlat * 12.4444);

        // calculate longitude scalar
        let scalarLon = 111320 * Math.cos(rlat);

        // convert to meters
        let deltaLat = Math.abs(lat2 - lat1) * scalarLat;
        let deltaLon = Math.abs(lon2 - lon1) * scalarLon;

        // return distance
        return Math.hypot(deltaLat, deltaLon);
    }

    export function meterToLon(meter: any, lon: any, lat: any) {

        // convert lat to radians
        let rlat = Trilateration.toRadians(lat);

        // calculate longitude scalar
        let scalarLon = 111320 * Math.cos(rlat);

        // return longitude
        return meter / scalarLon;
    }

    export function meterToLat(meter: any, lat: any) {

        // calculate latitude scalar
        let scalarLat = 110574 + (lat * 12.4444);

        // return latitude
        return meter / scalarLat;
    }

    export function toRadians(deg: any) {
        return (deg * Math.pi) / 180;
    }

    export function distanceLogTx(rssi,txPower) {
        let x = txPower - rssi;
        return Math.pow(10, x / 25);
        //return 10^(x/20)
    }

     function distanceLog(rssi) {
        let x = -47 - rssi;
        return Math.pow(10, x / 30);
        //return 10^(x/20)
    }
}

