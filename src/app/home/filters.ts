export interface LimitFilterInterface {
    getRawDistance() : number;
    getFilteredDistance(): number;
    setDistance(dist: number): void;

}

export interface LowPassFilterInterface {
    getRawRssi() : number;
    getFilteredRssi(): number;
    setRssi(rssi: number): void;

}

export class LowPassFilter implements LowPassFilterInterface{
    //private smoothingFactor  = 3;
    private smoothedValue: number = null;
    private rssi: number;
    private lastSeen: number;

    constructor(private smoothingFactor: number) {}

    getRawRssi(){
        return this.rssi;
    }

    getFilteredRssi(){
        return this.smoothedValue;
    }

    setRssi(rssi: number){
        this.rssi = rssi;
        const now = Date.now();

        if( this.smoothedValue){
            const timeSinceLastUpdate = (now-this.lastSeen)/1000;
            this.smoothedValue += timeSinceLastUpdate * (rssi - this.smoothedValue) / this.smoothingFactor;
        } else{
            this.smoothedValue = rssi;
        }

        this.lastSeen = now;

    }
}

export class LimiterFilter implements LimitFilterInterface{
    private filteredDistance: number;
    constructor(private maxSpeed:number, private smoothingFactor){}
    private timestamp: number;
    private rawDistance: number;
    private pu1: number = null;
    private pu2: number = null;
    private pu3:number = null;
    private pu4 :number = null;

    setDistance(dist: number) {

        // get raw distance
        const distance = dist;

        // get timestamp
        let dn = Date.now();

        // collect startup values
        if( this.pu1 == null) {
            this.pu1 = distance;
            return this.pu1;
        } else if( this.pu2 == null) {
            this.pu2=(this.pu1+distance)*0.5;
            return this.pu2;
        } else if( this.pu3==null) {
            this.pu3=(this.pu1+this.pu2+distance)*0.3333;
            return this.pu3;
        } else if( this.pu4==null) {
            this.timestamp=dn;
            this.pu4=(this.pu1+this.pu2+this.pu3+distance)*0.25;
            return this.pu4;
        }

        // calculate limiter value, set maxspeed to 2 ms
        let TL = this.maxSpeed/( 1000/(dn-this.timestamp) );
        this.timestamp = dn;

        // apply fraser filter
        let f1 = (this.pu1 + this.pu2) * 0.5;
        let f2 = (this.pu3 + this.pu4) * 0.5;
        let ev = f2+(f2-f1);

        // apply limiter filter
        let tv = distance-ev;
        tv = Math.min( tv, TL );
        tv = Math.max( tv, -TL );
        tv = tv/this.smoothingFactor;
        tv = tv+ev;

        // apply low pass filter
        let lv = (this.pu1+this.pu2+this.pu3+this.pu4+tv) * 0.2;

        // ready next round
        this.pu1=this.pu2;
        this.pu2=this.pu3;
        this.pu3=this.pu4;
        this.pu4=lv;

        // round value
        this.filteredDistance =  Math.round(this.pu4*10) * 0.1;
        this.rawDistance = dist;
    }

    getFilteredDistance(){
        return this.filteredDistance;
    }

    getRawDistance(){
        return this.rawDistance;
    }

}