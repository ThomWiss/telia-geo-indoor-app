import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {BLE} from '@ionic-native/ble/ngx';
import {GameengineServiceService} from "../gameengine-service.service";
import {Stepcounter} from '@ionic-native/stepcounter/ngx';
import {HttpResponse} from '@angular/common/http';
import {Circle, ILatLng,LatLng, GoogleMap, GoogleMaps, GoogleMapsAnimation, Marker, MyLocation} from '@ionic-native/google-maps';

import {LoadingController, Platform, ToastController} from '@ionic/angular';
import {Beacon} from "./beacon";
import {Trilateration} from "./trilateration";


function getLatLngFromGeoData(geoData: any) {
    return {lat: geoData.geoJson.geometry.coordinates[1], lng: geoData.geoJson.geometry.coordinates[0]};

}


@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
    devices: Map<string, any> = new Map(); // Scanned devices
    beacons: Map<string, Beacon> = new Map<string, Beacon>();
    gamerId: string = 'f1adaffd-bf3c-457d-b31a-5b9d26312636';
    steps: number = 0;
    public stageAppId = 'indoorstageteliaskepparegatan';
    public map: GoogleMap;
    myMarker: Marker;
    pois: Array<any> = [];
    googleMapsBeaconCircles: Map<string,Circle> = new Map();
    circle : Map<string,Circle> = new Map();
    scanning : boolean = false;


    constructor(
        private ble: BLE,
        private gameengineService: GameengineServiceService,
        private stepcounter: Stepcounter,
        private cd: ChangeDetectorRef,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
        private platform: Platform

    ) {}


    loadMap() {

        this.map = GoogleMaps.create('map_canvas');

        this.map.getMyLocation().then((location: MyLocation) =>{
            this.map.animateCamera({
                target: location.latLng,
                zoom: 16,
                tilt: 30
            });
            console.log(location.latLng);

            let map = this.map;
            const googleMapsBeaconCircles = this.googleMapsBeaconCircles;
            let beacons = this.beacons;
            this.gameengineService.getAllBeacons().then((res: HttpResponse<any>) =>{

                console.log(this.beacons);

                res.body.forEach(function(b){
                    console.log(b);
                    console.log(b.geoData.geoJson.geometry.coordinates);
                    let latLng = getLatLngFromGeoData(b.geoData);

                    let circle = map.addCircleSync({
                        'center': latLng,
                        'radius': 1,
                        'strokeColor' : '#FF0000',
                        'strokeWidth': 2,
                        'fillColor' : '#FF0000',
                        'strokeOpacity': 0.8,
                        'fillOpacity': 0.1,

                    });
                    googleMapsBeaconCircles.set(b.macAdress, circle);
                    beacons.set(b.macAdress, new Beacon(b.name,latLng.lat, latLng.lng, b.macAdress));

                });
            });



            this.myMarker = this.map.addMarkerSync({
                title: 'My position',
                //snippet: 'Limit filter pos',
                position: location.latLng,
                animation: GoogleMapsAnimation.BOUNCE,
                icon: 'green'
            });


        });


/*
        let mapOptions: GoogleMapOptions = {
            camera: {
                target: {
                    lat: 43.0741904,
                    lng: -89.3809802
                },
                zoom: 18,
                tilt: 30
            }
        };

        this.map = GoogleMaps.create('map_canvas', mapOptions);

        let marker: Marker = this.map.addMarkerSync({
            title: 'Ionic',
            icon: 'blue',
            animation: 'DROP',
            position: {
                lat: 43.0741904,
                lng: -89.3809802
            }
        });
        marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
            alert('clicked');
        });
        */
    }

    enterBeaconPoiRange(macAddress: string){
        console.log(macAddress);
        this.gameengineService.enterBeaconPoiRange(this.gamerId, macAddress)
            .then( res => {
                console.log(res);
                if(res.status ==204){
                    console.log("success");
                    let b = this.devices.get(macAddress);
                    b.entered = true;
                    this.devices.set(macAddress, b);

                }
            });
    }

    takeBeaconPoi(macAddress: string){
        this.gameengineService.takeBeaconPoi(this.gamerId, macAddress)
            .then(res =>{
                if(res.status ==204){
                    console.log("success");
                    let b = this.devices.get(macAddress);
                    b.taken = true;
                    this.devices.set(macAddress, b);

                }
            });
    }
    startScan() {
        this.scanning=true;
        console.log("start scan");
        //this.loadMap();
/*
        this.stepcounter.start(0).then(onSuccess => console.log('stepcounter-start success', onSuccess), onFailure => console.log('stepcounter-start error', onFailure));

        let map = this.map;
        this.gameengineService.flatStage(this.stageAppId).then(res => {
            console.log(res)
            this.pois = res.body.pois;
            //Draw pois
            this.pois.forEach(function (poi) {
                poi.geoData = res.body.geodatas.find(geo => geo.id == poi.geoDataId);
                let latLng = getLatLngFromGeoData(poi.geoData);

                map.addCircleSync({
                    'center': latLng,
                    'radius': poi.activationDistance,
                    //'strokeColor' : '#AA00FF',
                    'strokeWidth': 1,
                    'fillColor' : '#D3D3D3'
                })
            });
*/
            /*
            this.beacons = res.body.bleBeacons;
            this.beacons.forEach(function (beacon) {
                beacon.poi = res.body.pois.find(poi => poi.bleBeaconId == beacon.id);
            });
            console.log(this.beacons);
*/
            this.scan();
            //this.gameengineService.addImageOverlay();
       // });

    }

    stopScan() {
        this.scanning=false;
        console.log("stop scan");
        this.ble.stopScan();
    }


    private scan() {
        this.stepcounter.getHistory().then(historyObj => {
            console.log('stepcounter-history success', historyObj);
            this.steps = historyObj["2019-08-09"].steps;
            }, onFailure => console.log('stepcounter-history error', onFailure)
        );


        this.ble.startScanWithOptions([],{reportDuplicates: true}).subscribe(
            device => this.onDeviceDiscovered(device),
            error => this.scanError(error)
        );


    }

    onDeviceDiscovered(device) {


        if (device.name && device.name.includes("5AA")) {
            const foundDevice = this.beacons.get(device.id); // find(b => b.macAdress === device.id);

            if(foundDevice){
                console.log(device);
                const txPower = new Uint8Array(device.advertising)[29] - 256;
                //const distance = HomePage.distanceLog(device.rssi);
                // this.googleMapsBeaconCircles.get(device.id).setRadius(distance);
/*
                this.map.moveCamera({
                    target: this.googleMapsBeaconCircles.get(device.id).getCenter(),
                    zoom: 18,
                    tilt: 30
                });
*/
                foundDevice.setRssi(device.rssi,txPower);


                //this.beacons.set(device.id , foundDevice);

                let beacons = this.getLastSeenAndNearestBeacons();
                let pos = null;

                if(beacons.length >=3 ) {
                    //pos = Compute(beacons[0], beacons[1], beacons[2]);
                    pos = Trilateration.getPosition(beacons,this.myMarker.getPosition());
                }

                //let myposition = Trilateration.getPosition(beacons);
                console.log(device.id,foundDevice.getDistance(), device.rssi,pos, foundDevice.getRssis().length);


                if(pos) {
                    this.myMarker.setPosition(pos);
                   /*
                    this.myMarker.setSnippet(
                        beacons[0].name+": " +beacons[0].getDistance()+"\n"+
                        beacons[1].name+": " +beacons[1].getDistance()+"\n"+
                        beacons[2].name+": " +beacons[2].getDistance());
*/
                    /*
                    let slice = beacons.slice(0,3); //beacons used for position

                    console.log(slice);
                    console.log(slice.filter( b => b.getMacAddress() === device.id));

                    if(slice.filter( b => b.getMacAddress() === device.id).length >0){
                        this.googleMapsBeaconCircles.get(device.id).setRadius(foundDevice.getDistance());
                    }else{
                        this.googleMapsBeaconCircles.get(device.id).setRadius(0.5);
                    }*/

                }

              /*  if(foundDevice.getRssis().length === 128){
                    //this.ble.stopScan();
                    //console.log(foundDevice.getRssis());
                    let s = new Spectrum(128);
                    s.appendData(foundDevice.getRssis());
                    s.recompute();
                    console.log(s.getPower());
                    console.log(s.getPhase());
                }
*/




            }/*else{
                console.log("not found.",device);
            }*/

            /*if(foundDevice){
                console.log("FOund beacon", foundDevice);

                let txPower = new Uint8Array(device.advertising)[29]-256;
                console.log(txPower);
                let distance = this.distanceLog(device.rssi, txPower);

                if (this.devices.has(device.id)) {

                    let dev = this.devices.get(device.id);
                    dev.rssi = device.rssi;
                    dev.distance = distance;
                    this.devices.set(device.id,dev);
                }
                else {
                    device.displayName = foundDevice.name;
                    device.distance = distance;
                    //device.activationDistance = foundDevice.poi.activationDistance;
                    device.entered = false;
                    device.taken = false;
                    this.devices.set(device.id, device);
                }

                this.cd.detectChanges();
            }*/

        }

    }

    setStatus(message) {
        console.log(message);
    }

    scanError(error) {
        console.log(error);
        this.setStatus('Error ' + error);
    }

    getDevices(){
        return Array.from(this.devices.values()).sort((a, b) => b.rssi - a.rssi);
    }

    getPois(){
        //GeoLib.getDistance()
        //this.myMarker.
        return Array.from(this.pois.values()).sort((a, b) => b.rssi - a.rssi);
    }


    ngOnInit(): void {
        this.loadMap();
        console.log("init");

        this.stepcounter.start(0).then(onSuccess => console.log('stepcounter-start success', onSuccess), onFailure => console.log('stepcounter-start error', onFailure));

        let map = this.map;
        this.gameengineService.flatStage(this.stageAppId).then(res => {
            console.log(res)
            this.pois = res.body.pois;
            //Draw pois
            this.pois.forEach(function (poi) {
                poi.geoData = res.body.geodatas.find(geo => geo.id == poi.geoDataId);
                let latLng = getLatLngFromGeoData(poi.geoData);

                map.addCircleSync({
                    'center': latLng,
                    'radius': poi.activationDistance,
                    //'strokeColor' : '#AA00FF',
                    'strokeWidth': 1,
                    'fillColor': '#D3D3D3'
                })
            });

            res.body.fences.forEach(function(fence){
               console.log(fence);
                let geodata = res.body.geodatas.find(geo => geo.id == fence.geoDataId);
                console.log(geodata);
                let polygon: ILatLng[]=[];
                geodata.geoJson.geometry.coordinates[0].forEach(coords =>{
                    console.log(coords);
                        polygon.push(new LatLng(coords[1],coords[0]));
                });
                console.log(polygon);
                map.addPolygon({points:polygon});
            });

            let jsonExtension = JSON.parse(res.body.stage.jsonExtension);
            console.log(jsonExtension);
            let fl1Bounds = jsonExtension.fl1.bounds;
            console.log(fl1Bounds);
            let bounds: ILatLng[] = new Array();
            bounds.push(new LatLng(fl1Bounds.swLt, fl1Bounds.swLg));
            bounds.push(new LatLng(fl1Bounds.neLt, fl1Bounds.neLg));

            let floor = 1;

            this.gameengineService.addImageOverlay(this.stageAppId, map, bounds, floor, jsonExtension.fl1.rCw);
        });

    }

    getLastSeenAndNearestBeacons(): Array<Beacon> {
        let filter = Array.from(this.beacons.values()).filter(b =>   b.getLastSeen() > Date.now() - 6000);
        return filter.sort((a, b) => a.getDistance() - b.getDistance());
    }

}
