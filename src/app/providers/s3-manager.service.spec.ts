import { TestBed } from '@angular/core/testing';

import { S3ManagerService } from './s3-manager.service';

describe('S3ManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: S3ManagerService = TestBed.get(S3ManagerService);
    expect(service).toBeTruthy();
  });
});
