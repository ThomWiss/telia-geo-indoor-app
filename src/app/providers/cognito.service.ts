import { Injectable } from '@angular/core';
import * as AWSCognito from 'amazon-cognito-identity-js';


@Injectable({
  providedIn: 'root'
})
export class CognitoService {

    private _POOL_DATA = {
        UserPoolId: "eu-west-1_k3MOOUrlE",
        ClientId: "15aaddnhc6hgge3306e2nd17t4"
    };


  constructor() { }

   getToken(username: string, password: string) :Promise<string>{
    const userPool = new AWSCognito.CognitoUserPool(this._POOL_DATA);

    const authDetails = new AWSCognito.AuthenticationDetails({
        Username: username,
        Password: password
    });

    const cognitoUser = new AWSCognito.CognitoUser({
        Username: username,
        Pool: userPool
    });

    return new Promise(function(resolve, reject){

        cognitoUser.authenticateUser(authDetails,{
            onSuccess: res => {
                //console.log(res);
                //console.log(res.getAccessToken().getJwtToken());
                resolve(res.getIdToken().getJwtToken());
            },
            onFailure: err => {
              console.log(err);
              reject(err);
            }
        });

    });


  }

}
