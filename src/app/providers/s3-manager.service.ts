import { Injectable } from '@angular/core';
import * as AWS from 'aws-sdk';
import {GoogleMap,ILatLng, GroundOverlay} from '@ionic-native/google-maps';
@Injectable({
  providedIn: 'root'
})
export class S3ManagerService {

  constructor() { }


    public createOverlay(token:string, applicationId: string, floor:number, map: GoogleMap, bounds: ILatLng[], bearing: number = 0)
    {

        var credentials = new AWS.CognitoIdentityCredentials({ IdentityPoolId: 'eu-west-1:08301d1d-369d-4ba3-819b-44e3c5a3df35', Logins: {'cognito-idp.eu-west-1.amazonaws.com/eu-west-1_k3MOOUrlE': token} });

        AWS.config.credentials = credentials;
        AWS.config.region = 'eu-west-1';
        var s3 = new AWS.S3({region: 'eu-north-1'});
        let prefix = applicationId+'/'+floor+'/';

        let params = {
            Bucket: 'game-engine-theme-storage',
            Delimiter: '/',
            Prefix: prefix //'indoorstageteliaskepparegatan/1/'

        };

        s3.listObjects(params, function(error, data)
        {
            if (error)
                console.log(error, error.stack); // an error occurred

            else {
                console.log(JSON.stringify(data));
                let filename = data.Contents[0].Key;
                console.log(filename);

                s3.getObject({Bucket: 'game-engine-theme-storage', Key: filename}, function (err, data) {
                    console.log(err);
                    console.log(data);

                    // Add ground overlay
                    console.log(map);
                    console.log(bounds);

                    const binstr = Array.prototype.map.call(
                        data.Body,
                        ch => String.fromCharCode(ch)
                    ).join('');
                    const b64encoded = btoa(binstr);
                    const previewData = `data:image/png;base64,${b64encoded}`;

                    console.log("bearing: ", bearing);
                    map.addGroundOverlay({
                        'url': previewData,
                        'bounds': bounds,
                        'opacity': 0.5,
                        'bearing': bearing
                    })
                        .then(success => console.log(success), error => console.log(error))
                        .catch(err => console.log(err));


                })
            }
        });


    }
}
