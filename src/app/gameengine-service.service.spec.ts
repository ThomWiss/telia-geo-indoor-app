import { TestBed } from '@angular/core/testing';

import { GameengineServiceService } from './gameengine-service.service';

describe('GameengineServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GameengineServiceService = TestBed.get(GameengineServiceService);
    expect(service).toBeTruthy();
  });
});
