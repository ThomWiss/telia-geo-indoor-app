import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CognitoService} from "./providers/cognito.service";
import {S3ManagerService} from "./providers/s3-manager.service";
import { GoogleMap, ILatLng} from '@ionic-native/google-maps';

@Injectable({
  providedIn: 'root'
})
export class GameengineServiceService {
  private flatStageurl = "https://thmo40p1i0.execute-api.eu-west-1.amazonaws.com/beta/api/game-app/stages";
  private gameUrl = "https://thmo40p1i0.execute-api.eu-west-1.amazonaws.com/beta/api/game-app";
  private beaconsUrl = "https://thmo40p1i0.execute-api.eu-west-1.amazonaws.com/beta/api/game-app/ble-beacons-with-geo-data";

  private token : any = null;

  constructor(protected  http: HttpClient, protected cognitoService: CognitoService, protected s3ManagerService: S3ManagerService) { }

  async flatStage(appId: string) {
    await this.getToken();
    console.log(this.token);

    return  this.http.get<any>(`${this.flatStageurl}/${appId}`, {
        headers: {"Authorization": "Bearer " + this.token},
        observe: 'response'
    }).toPromise();
  }

   enterBeaconPoiRange(gamerId: string, macAddress: string){
      return this.http.post(`${this.gameUrl}/enterBeaconPoi`, {gamerId: gamerId, macAddress: macAddress},{
          headers: {"Authorization": "Bearer " + this.token},
          observe: 'response'
      }).toPromise();

  }

    takeBeaconPoi(gamerId: string, macAddress: string){
        return this.http.post(`${this.gameUrl}/takeBeaconPoi`, {gamerId: gamerId, macAddress: macAddress},{
            headers: {"Authorization": "Bearer " + this.token},
            observe: 'response'
        }).toPromise();

    }

    async getAllBeacons(){
        await this.getToken();
        return  this.http.get<any>(`${this.beaconsUrl}`, {
            headers: {"Authorization": "Bearer " + this.token},
            observe: 'response'
        }).toPromise();
    }



    async addImageOverlay(applicationId: string, map:GoogleMap, bounds: ILatLng[], floor:number = 0, bearing: number = 0){
      await this.getToken();
      this.s3ManagerService.createOverlay(this.token, applicationId, floor, map,bounds, bearing );
    }

    private async getToken() {
        if (this.token == null) {
            this.token = await this.cognitoService.getToken("IonicAppUser", "IonicAppUser2019");
        }
    }

}
